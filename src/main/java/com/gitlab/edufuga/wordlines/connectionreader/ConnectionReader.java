package com.gitlab.edufuga.wordlines.connectionreader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class ConnectionReader {
   private final Path connectionFolder;

   public ConnectionReader(String connectionFolder) {
      this(Paths.get(connectionFolder));
   }

   public ConnectionReader(Path connectionFolder) {
      this.connectionFolder = connectionFolder;
   }

   public Map<String, List<String>> read(String language, String word) throws IOException {
      List<Path> connections = findConnections(language, word);

      // Parse a bit
      Map<String, List<String>> result = new HashMap<>();
      connections.forEach((Path path) -> {
         String filename = path.getFileName().toString();

         // TODO: Actually there is no need to read the file name. The folders in the path are enough information.

         String[] strings = filename.split("_");
         String fromLanguage = strings[0];
         String fromWord = strings[1];
         String connectionLanguage = strings[2];
         String connection = strings[3];
         String toLanguage = strings[4];
         String toWord = strings[5];

         fromWord = fromWord.replace("]", "");
         fromWord = fromWord.replace(".tsv", "");
         toWord = toWord.replace("]", "");
         toWord = toWord.replace(".tsv", "");

         String from = fromLanguage + "_" + fromWord;
         String to = toLanguage + "_" + toWord;

         // Pseudo-free-style connection message.
         String connectionMessage = fromLanguage+"/"+fromWord+"\t"+connectionLanguage+"/"+connection+"\t"+toLanguage+"/"+toWord;

         result.putIfAbsent(from, new ArrayList<>());
         result.get(from).add(connectionMessage);
      });

      return result;
   }

   private List<Path> findConnections(String language, String word) throws IOException {
      Path connectionsFolderForWordOfLanguage = connectionFolder.resolve(language).resolve(word);

      if (Files.notExists(connectionsFolderForWordOfLanguage)) {
         return Collections.emptyList();
      }

      return Files.walk(connectionsFolderForWordOfLanguage, 5)
              .filter(Files::isRegularFile)
              .collect((Collectors.toList()));
   }

   public static void main(String[] args) {
      System.out.println("This is the ConnectionReader. It doesn't do anything useful yet.");
   }
}

